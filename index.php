<?php
#================================ setting =====================================#
define ('HOST', '');
#==============================================================================#
if ($_REQUEST) {
	writeToLog($_REQUEST, 'реквест в норе');

	$id = $_REQUEST['data']['FIELDS']['ID'];
	$activity = call ('crm.activity.get', array('ID' => $id));
	writeToLog($activity, 'activity');

	$sub = preg_match('~ID Заявки -~', $activity['result']['SUBJECT']);
	if (!$sub) {
		writeToLog($sub, 'Зашли на апдейт');
		$updateActivity = call('crm.activity.update', array(
			'id' => $activity['result']['ID'],
			'fields' => array(
				'SUBJECT' => $activity['result']['SUBJECT'].' | ID Заявки - '.$activity['result']['OWNER_ID'],
				// 'DESCRIPTION' => $activity['result']['DESCRIPTION']. '| ID Заявки - '.$activity['result']['OWNER_ID']
			)
		));
	}
	writeToLog($updateActivity, 'Апдейт заголовка');

	if ($activity['result']['TYPE_ID'] != '4') die();

	switch ($activity['result']['OWNER_TYPE_ID']) {
		case '1':
			$type = 'lead';
			$activityInfo = call ('crm.lead.get', array('ID' => $activity['result']['OWNER_ID']));
			$contact_id = $activityInfo['result']['CONTACT_ID'] ? $activityInfo['result']['CONTACT_ID'] : '';
			break;
		
		case '3':
			$type = 'contact';
			$activityInfo = call ('crm.contact.get', array('ID' => $activity['result']['OWNER_ID']));
			$contact_id = $activityInfo['result']['ID'];
			break;

		case '4':
			$type = 'company';
			$activityInfo = call ('crm.company.get', array('ID' => $activity['result']['OWNER_ID']));
			$company_id = $activityInfo['result']['COMPANY_ID'];
			break;

		case '2':
			$type = 'deal';
			$activityInfo = call ('crm.deal.get', array('ID' => $activity['result']['OWNER_ID']));
			$contact_id = $activityInfo['result']['CONTACT_ID'];
			break;

		default:
			die();
	}
	writeToLog($activityInfo, 'activityInfo');

	$tmp_email = explode(' ', $activity['result']['SETTINGS']['EMAIL_META']['replyTo']);
	$email_del1 = trim(array_pop($tmp_email), '<');
	$email = trim($email_del1, '>');

	if ($activity['result']['DIRECTION'] != '2') {
		$bool = preg_match('~ID Заявки~',  $activity['result']['SUBJECT']);
		writeToLog($bool, 'bool');

		$date = explode('T', $activityInfo['result']['DATE_CREATE']);
		$delay = getDelay($date[0]);
		if ($delay == 0) {
			$time = explode(':', $date[1]);
			$h = date('H') - $time[0];
			$m = date('i') - $time[1];
			if ($h > 0 || ($h == 0 && $m > 1)) $boolean = true;
		} else { $boolean = true; }

		if (!$bool && $boolean) {
			writeToLog('переходим к созданию лидов и дела');

			$leadData = array(
				'TITLE'                => $activity['result']['SUBJECT'],
				'NAME'                 => $activityInfo['result']['NAME'] ? $activityInfo['result']['NAME'] : '',
				'SECOND_NAME'          => $activityInfo['result']['SECOND_NAME'] ? $activityInfo['result']['SECOND_NAME'] : '',
				'STATUS_ID'            => 'NEW',
				'OPENED'               => 'Y',
				'ASSIGNED_BY_ID'       => $activityInfo['result']['ASSIGNED_BY_ID'],
				'COMPANY_ID'           => $company_id,
				'CONTACT_ID'           => $contact_id,
				'EMAIL'                => array(array('VALUE' => $email)),
				'UF_CRM_1571823403'    => 'Y',
				'SOURCE_ID'            => 'EMAIL'
			);

			writeToLog($leadData, 'массив для лида');
			$lead = call ('crm.lead.add', array('fields' => $leadData));
			writeToLog($lead, 'новый лид');

			// if (!$sub = preg_match('~ID Заявки -~', $activity['result']['SUBJECT'])) {
			// 	$updateActivity = call('crm.activity.update', array(
			// 		'id' => $activity['result']['ID']
			// 		'fields' => array(
			// 			'SUBJECT' => $activity['result']['SUBJECT'].' | ID Заявки - '.$lead['result']
			// 		)
			// 	));
			// }

			$newActivityData = array( 'fields' =>array(
	            "OWNER_ID"             => $lead    ['result'],
	            "OWNER_TYPE_ID"        => '1',
	            "TYPE_ID"              => $activity['result'][/***************/'TYPE_ID'],
	            "PROVIDER_ID"          => $activity['result'][/***********/'PROVIDER_ID'],
	            "PROVIDER_TYPE_ID"     => $activity['result'][/******/'PROVIDER_TYPE_ID'],
	            "ASSOCIATED_ENTITY_ID" => $activity['result'][/**/'ASSOCIATED_ENTITY_ID'],
	            "SUBJECT"              => $activity['result'][/***************/'SUBJECT']. ' | ID Заявки - '.$lead['result'],
	            "CREATED"              => $activity['result'][/***************/'CREATED'],
	            "LAST_UPDATED"         => $activity['result'][/**********/'LAST_UPDATED'],
                "START_TIME"           => $activity['result'][/************/'START_TIME'],
                "END_TIME"             => $activity['result'][/**************/'END_TIME'],
	            "COMMUNICATIONS"       => array( array( 'VALUE'=> $activity['result']['SETTINGS']['EMAIL_META']['from'])),
	            "COMPLETED"            => 'N',
	            "STATUS"               => $activity['result'][/****************/'STATUS'],
	            "RESPONSIBLE_ID"       => $activity['result'][/********/'RESPONSIBLE_ID'],
	            "PRIORITY"             => $activity['result'][/**************/'PRIORITY'],
	            "DESCRIPTION"          => $activity['result'][/***********/'DESCRIPTION'],
	            "DESCRIPTION_TYPE"     => $activity['result'][/******/'DESCRIPTION_TYPE'],
	            "DIRECTION"            => '1',
	            "SETTINGS"             => $activity['result'][/**************/'SETTINGS'],
	            "AUTHOR_ID"            => $activity['result'][/*************/'AUTHOR_ID'],
	            "EDITOR_ID"            => $activity['result'][/*************/'EDITOR_ID'],
	            "RESULT_MARK"          => $activity['result'][/***********/'RESULT_MARK'],
                "RESULT_STATUS"        => $activity['result'][/*********/'RESULT_STATUS'],
                "RESULT_STREAM"        => $activity['result'][/*********/'RESULT_STREAM'],
                "AUTOCOMPLETE_RULE"    => $activity['result'][/*****/'AUTOCOMPLETE_RULE'],
                "NOTIFY_TYPE"          => '0',
                "NOTIFY_VALUE"         => '0'
	            )
			);
			writeToLog($newActivityData, 'массив для дела');
			$newActivity = call('crm.activity.add', $newActivityData);
			writeToLog($newActivity, 'новое дело');

			$del = call ('crm.activity.delete', array('ID' => $activity['result']['ID']));
			writeToLog($del, 'del activity');
		}
	}
}

################################################################################
############################### functions ######################################
function call ($method, $post) {
    $result = file_get_contents(HOST.$method.'?'.http_build_query($post));
    return json_decode($result, true);
}

function writeToLog ($data, $title = 'DEBUG') {
	$log = "\n--------------------\n";
	$log .= date('d.m.Y H:i:s')."\n";
	$log .= $title."\n";
	$log .= print_r ($data, 1);
	$log .= "\n--------------------\n";

	file_put_contents(__DIR__.'/degub.txt', $log, FILE_APPEND);
	return true;
}

function getDelay ($checkX) {
	$checkInX = explode('-', $checkX);
	$date = mktime(0,0,0, $checkInX[1],$checkInX[2],$checkInX[0]);
	$today  = mktime(0, 0, 0, date("m")  , date("d"), date("Y"));
	$delay = $today - $date;
	return $delay;
}